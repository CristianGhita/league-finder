package com.roplus.finalleaguefinder;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ActivityUserClicked extends AppCompatActivity {

    String name,tier,division,Uid,team,MyTeamName;
    String[] tierLocal = {"BRONZE","SILVER","GOLD","PLATINUM","DIAMOND","MASTER","CHALLENGER"};
    int[] tierLocalInt = {
            R.drawable.bronze,
            R.drawable.silver,
            R.drawable.gold,
            R.drawable.platinum,
            R.drawable.diamond,
            R.drawable.master,
            R.drawable.challenger
    };

    TextView textViewNameUC, textViewRankUC;
    ImageView imageViewRankUC;
    Button buttonInviteUC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_clicked);

        final LinearLayout linearLayoutButtonInviteUC = (LinearLayout) findViewById(R.id.linearLayoutButtonInviteUC);

        textViewNameUC = (TextView) findViewById(R.id.textViewNameUC);
        textViewRankUC = (TextView) findViewById(R.id.textViewRankUC);
        imageViewRankUC = (ImageView) findViewById(R.id.imageViewRankUC);
        buttonInviteUC = (Button) findViewById(R.id.buttonInviteUC);

        Bundle extras = getIntent().getExtras();
        if (extras!=null){
            name = extras.getString("name");
            tier = extras.getString("tier");
            division = extras.getString("division");
            Uid = extras.getString("Uid");
            team = extras.getString("team");
            MyTeamName = extras.getString("MyTeamName");
        }

        if (team.equals("yes")){
            linearLayoutButtonInviteUC.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        } else {
            linearLayoutButtonInviteUC.setLayoutParams(new LinearLayout.LayoutParams(0,0));
        }

        for ( int i = 0; i<tierLocal.length;i++){
            if (tier.equals(tierLocal[i])){
                textViewNameUC.setText(name);
                textViewRankUC.setText(tier+" "+division);
                imageViewRankUC.setImageResource(tierLocalInt[i]);
                break;
            }
        }

        buttonInviteUC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

                mDatabase.child("users").child(Uid).child("invites").setValue(MyTeamName);
                mDatabase.child("teams").child(MyTeamName).child("invited").child(Uid).setValue("pending");

                Toast.makeText(getBaseContext(),"Invitation sent",Toast.LENGTH_SHORT).show();
                buttonInviteUC.setText("Invitation sent");
            }
        });
    }
}
