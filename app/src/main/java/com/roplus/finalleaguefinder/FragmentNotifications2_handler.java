package com.roplus.finalleaguefinder;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNotifications2_handler extends Fragment {


    public FragmentNotifications2_handler() {
        // Required empty public constructor
    }


    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    Team team;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_fragment_notifications, container, false);

        MainActivity activity = (MainActivity) getActivity();
        final User user = activity.getUser();

        LinearLayout linearLayoutNoInvitations = (LinearLayout) view.findViewById(R.id.linearLayoutNoInvitations);
        LinearLayout linearLayoutInvitations = (LinearLayout) view.findViewById(R.id.linearLayoutInvitations);

        final Button buttonAccept = (Button) view.findViewById(R.id.buttonAccept);
        final Button buttonDecline = (Button) view.findViewById(R.id.buttonDecline);

        try{
            Log.d("invites",user.getInvites());
            linearLayoutInvitations.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            linearLayoutNoInvitations.setLayoutParams(new LinearLayout.LayoutParams(0,0));

            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("teams");

            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot mydata : dataSnapshot.getChildren()) {
                        team = mydata.getValue(Team.class);
                        if (user.getInvites().equals(team.getName())){
                            Log.e("team is set",team.getName());
                            break;
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    buttonAccept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDatabase.child("users").child(user.getUid()).child("invites").removeValue();
                            mDatabase.child("users").child(user.getUid()).child("team").setValue(team.getName());

                            ArrayList<String> members = team.getMembers();
                            members.add(user.getCsSummonerName());

                            int size = team.getSize();
                            size += 1;

                            mDatabase.child("teams").child(team.getName()).child("size").setValue(size);
                            mDatabase.child("teams").child(team.getName()).child("members").setValue(members);

                            MainActivity activity = (MainActivity) getActivity();
                            activity.update(view);
                        }
                    });

                    buttonDecline.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDatabase.child("users").child(user.getUid()).child("invites").removeValue();
                        }
                    });

                    TextView textViewTeamNameNot = (TextView) view.findViewById(R.id.textViewTeamNameNot);
                    textViewTeamNameNot.setText(team.getName()); // //
                }
            }, 2000);



        }catch(Exception e){
            Log.e("invites","missing");
            linearLayoutNoInvitations.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            linearLayoutInvitations.setLayoutParams(new LinearLayout.LayoutParams(0,0));
        }

        return view;
    }

}
