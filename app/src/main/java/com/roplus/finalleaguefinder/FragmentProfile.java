package com.roplus.finalleaguefinder;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Contacts;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentProfile extends Fragment {


    public FragmentProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v =  inflater.inflate(R.layout.fragment_fragment_profile, container, false);

        MainActivity activity = (MainActivity) getActivity();
        final User user = activity.getUser();
        final Team team = activity.getTeam();

        Log.e("ProfileTeam",team.getName());

        final EditText editTextTeamName = (EditText) v.findViewById(R.id.editTextTeamName);
        final TextView textViewPlayer1 = (TextView) v.findViewById(R.id.textViewPlayer1);
        final TextView textViewTeamName = (TextView) v.findViewById(R.id.textViewTeamName);

        int[] textViews = {
                R.id.textViewPlayer2,
                R.id.textViewPlayer3,
                R.id.textViewPlayer4,
                R.id.textViewPlayer5
        }; // // TODO: 11-Dec-16 termina aici

        Log.e("Members", String.valueOf(team.getMembers()));

        for (int i = 1; i<team.getSize();i++){
            TextView textView = (TextView) v.findViewById(textViews[i-1]);
            textView.setText(team.getMembers().get(i));
        }

        final LinearLayout linearLayoutTeam = (LinearLayout) v.findViewById(R.id.linearLayoutTeam);
        final LinearLayout linearLayoutCreate = (LinearLayout) v.findViewById(R.id.linearLayoutCreate);

        Log.e("all", "good");

        try {
            Log.e("team", user.getTeam());

            linearLayoutCreate.setLayoutParams(new LinearLayout.LayoutParams(0,0));
            linearLayoutTeam.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            textViewTeamName.setText(user.getTeam());

            textViewPlayer1.setText(team.getCaptain());

        } catch (Exception e) {
            Log.e("team", "null");
            final Button buttonCreate = (Button) v.findViewById(R.id.buttonCreate);
            buttonCreate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final String newTeamName = editTextTeamName.getText().toString();

                    DatabaseReference mDatabase;
                    mDatabase = FirebaseDatabase.getInstance().getReference();

                    mDatabase.child("teams").child(newTeamName).child("name").setValue(newTeamName);
                    mDatabase.child("teams").child(newTeamName).child("size").setValue(1);
                    mDatabase.child("teams").child(newTeamName).child("captain").setValue(user.getCsSummonerName());

                    ArrayList<String> members = new ArrayList<String>();
                    members.add(user.getCsSummonerName());

                    mDatabase.child("teams").child(newTeamName).child("members").setValue(members);

                    mDatabase.child("users").child(user.getUid()).child("team").setValue(newTeamName);

                    linearLayoutCreate.setLayoutParams(new LinearLayout.LayoutParams(0,0));

                    linearLayoutTeam.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    textViewPlayer1.setText(user.getCsSummonerName());
                    textViewTeamName.setText(newTeamName);
                }
            });
        }
        
        Button buttonLeave = (Button) v.findViewById(R.id.buttonLeave);
        buttonLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.child("users").child(user.getUid()).child("team").removeValue();

                final FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("teams");

                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot mydata : dataSnapshot.getChildren()) {
                            Team team = mydata.getValue(Team.class);

                            Log.e("members", String.valueOf(team.getMembers()));
                            Log.e("team",team.getName());
                            if (team.getMembers().contains(user.getCsSummonerName())) {
                                if(team.getSize()==1){
                                    mDatabase.child("teams").child(team.getName()).removeValue();
                                    Toast.makeText(v.getContext(),"You left team "+team.getName(),Toast.LENGTH_SHORT).show();
                                } else {
                                    mDatabase.child("teams").child(team.getName()).child("size").setValue(team.getSize()-1);
                                    for (int i = 0; i<team.getSize(); i++){
                                        if (team.getMembers().get(i).equals(user.getCsSummonerName())){
                                            mDatabase.child("teams").child(team.getName()).child("memebers").child(String.valueOf(i)).removeValue();
                                            break;
                                        }
                                    }
                                }


                            } else {
                                Log.e("team", "error - team not found");
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });



        return v;
    }

}
