package com.roplus.finalleaguefinder;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSearch extends Fragment {

    public FragmentSearch() {
        // Required empty public constructor
    }

    private static final String FILE_NAME = "list.txt";
    private File file;
    private FileOutputStream outputStream;
    private FileInputStream inputStream;

    int[] decision = {0,0,0,0,0};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment_search, container, false);

        file = new File(getContext().getFilesDir(), FILE_NAME);

        //stackoverflow only



        final int[] tierPhotos = {
                R.drawable.bronze,
                R.drawable.bronze,
                R.drawable.silver,
                R.drawable.gold,
                R.drawable.platinum,
                R.drawable.diamond,
                R.drawable.master,
                R.drawable.challenger
        };

        final String[] tier = {"MinTier","Bronze", "Silver", "Gold", "Platinum", "Diamond", "Master", "Challenger"};
        final String[] tierMaxSA = {"MaxTier","Bronze", "Silver", "Gold", "Platinum", "Diamond", "Master", "Challenger"}; // SA - String Array
        final String[] division = {"MinDiv","V", "IV", "III", "II", "I"};
        final String[] divisionMaxSA = {"MaxDiv","V", "IV", "III", "II", "I"}; // SA - String Array

        Spinner spinnerPlayersOrTeams = (Spinner) v.findViewById(R.id.spinnerPlayersOrTeams);
        Spinner spinnerTierMin = (Spinner) v.findViewById(R.id.spinnerTierMin);
        final Spinner spinnerDivisionMin = (Spinner) v.findViewById(R.id.spinnerDivisionMin);

        final Spinner spinnerTierMax = (Spinner) v.findViewById(R.id.spinnerTierMax);
        final Spinner spinnerDivisionMax = (Spinner) v.findViewById(R.id.spinnerDivisionMax);

        ArrayAdapter<CharSequence> adapterPlayerOrTeams = ArrayAdapter.createFromResource(this.getActivity(),   R.array.playersorteams, R.layout.spinner_search);
        ArrayAdapter<String> adapterTierMin = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_search, tier);
        final ArrayAdapter<String> adapterDivisionMin = new ArrayAdapter<String>(this.getActivity(), R.layout.spinner_search, division);
        //ArrayAdapter<CharSequence> adapterTier = ArrayAdapter.createFromResource(this.getActivity(), R.array.tier, R.layout.spinner_search);
        //final ArrayAdapter<CharSequence> adapterDivision = ArrayAdapter.createFromResource(this.getActivity(), R.array.division, R.layout.spinner_search);

        adapterPlayerOrTeams.setDropDownViewResource(android.R.layout.simple_spinner_item);
        adapterTierMin.setDropDownViewResource(android.R.layout.simple_spinner_item);
        adapterDivisionMin.setDropDownViewResource(android.R.layout.simple_spinner_item);

        spinnerPlayersOrTeams.setAdapter(adapterPlayerOrTeams);
        spinnerTierMin.setAdapter(adapterTierMin);
        spinnerDivisionMin.setAdapter(adapterDivisionMin);

        final ListView listViewSearch = (ListView) v.findViewById(R.id.listViewSearch);

        final Activity activity = this.getActivity();

        spinnerPlayersOrTeams.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("onItemClick", (String) parent.getItemAtPosition(position));
                if (position != '0'){
                    decision[0] = position;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.e("onNothingSelected", "NOOOOOOOOOTHING");
            }
        });
        spinnerTierMin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public Context getActivity() {
                return activity;
            }

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("onItemClickTier", (String) parent.getItemAtPosition(position));
                decision[1] = position;
                Log.e("position", String.valueOf(position));
                if (parent.getItemAtPosition(position).equals("Challenger") || parent.getItemAtPosition(position).equals("Master")){
                    ArrayAdapter<CharSequence> adapterDivision1 = ArrayAdapter.createFromResource(activity, R.array.division1, R.layout.spinner_search);
                    spinnerDivisionMin.setAdapter(adapterDivision1);
                } else {
                    spinnerDivisionMin.setAdapter(adapterDivisionMin);
                }

                ArrayList<String> tierMax = new ArrayList<String>();
                for( int i = position; i<tierMaxSA.length; i++){
                    tierMax.add(tierMaxSA[i]);
                }
                ArrayAdapter<String> adapterTierMax = new ArrayAdapter(this.getActivity(), R.layout.spinner_search, tierMax);
                adapterTierMax.setDropDownViewResource(android.R.layout.simple_spinner_item);
                spinnerTierMax.setAdapter(adapterTierMax);

                spinnerTierMax.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Log.e("tierMax", (String) parent.getItemAtPosition(position));
                        decision[3] = position+decision[1];
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerDivisionMin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public Context getActivity() {
                return activity;
            }

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("onItemClickDivision", (String) parent.getItemAtPosition(position));
                decision[2] = position-1;

                ArrayList<String> divisionMax = new ArrayList<String>();
                for( int i = position; i<divisionMaxSA.length; i++){
                    divisionMax.add(divisionMaxSA[i]);
                }
                ArrayAdapter<String> adapterDivisionMax = new ArrayAdapter(this.getActivity(), R.layout.spinner_search, divisionMaxSA);
                adapterDivisionMax.setDropDownViewResource(android.R.layout.simple_spinner_item);
                spinnerDivisionMax.setAdapter(adapterDivisionMax);

                spinnerDivisionMax.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Log.e("divisionMax", (String) parent.getItemAtPosition(position));
                        decision[4] = position-1;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        
        
        Button buttonSearch = (Button) v.findViewById(R.id.buttonSearch);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                //display the listview

                final String[] tierDisplay = {"None", "BRONZE", "SILVER", "GOLD", "PLATINUM", "DIAMOND", "MASTER", "CHALLENGER"};
                final String[] divDisplay = {"V", "IV", "III", "II", "I"};

                final String[] tierDisplay2 = new String[decision[3]-decision[1]+1];// tier display2 higher than 2

                Log.e("decision[1]", String.valueOf(decision[1]));
                Log.e("decision[3]", String.valueOf(decision[3]));

                for (int i = decision[1], j=0; i<=decision[3]; i++, j++){
                    tierDisplay2[j] = tierDisplay[i];
                    Log.e("tierDisplay2["+j+"]", tierDisplay2[j]);
                }
                for (int i = 0; i<decision.length; i++){
                    Log.e("decision["+i+"]", String.valueOf(decision[i]));
                }

                final FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference("users");

                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        int userDivision = 0;
                        int userTier = 0;
                        final ArrayList<Summoner> summoners = new ArrayList<>();
                        final ArrayList<User> displayedUsers = new ArrayList<>();

                        for(DataSnapshot mydata : dataSnapshot.getChildren()){ // adds all the summoners to the ArrayList<Summoner> summoners
                            User user = mydata.getValue(User.class);

                            for (int i = 0; i<divDisplay.length; i++){
                                if (user.getDivision().equals(divDisplay[i])){
                                    userDivision = i;
                                    break;
                                }
                            }
                            for (int i =0; i<tierDisplay.length; i++){
                                if (user.getTier().equals(tierDisplay[i])){
                                    userTier = i;
                                    break;
                                }
                            }

                            if(Arrays.asList(tierDisplay2).contains(user.getTier())){
                                //right tier
                                if ((userTier == decision[1] && userDivision >= decision[2])
                                        || (userTier == decision[3] && userDivision <= decision[4])
                                        || (userTier > decision[1] && userTier < decision[3])){
                                    // right division
                                    summoners.add(new Summoner(user.getCsSummonerName(), user.getTier()+" "+user.getDivision(), tierPhotos[userTier]));
                                    displayedUsers.add(user);

                                }
                            }

                        }

                        listViewSearch.setAdapter(new PlayerSearchAdapter(v.getContext(), R.layout.search_item_player, summoners));
                        listViewSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Toast.makeText(getContext(), "you clicked "+summoners.get(position), Toast.LENGTH_SHORT).show();

                                String nameToPass = displayedUsers.get(position).getCsSummonerName();
                                String tierToPass = displayedUsers.get(position).getTier();
                                String divisionToPass = displayedUsers.get(position).getDivision();
                                String UidToPass = displayedUsers.get(position).getUid();

                                Intent intent = new Intent(v.getContext(),ActivityUserClicked.class);
                                intent.putExtra("name",nameToPass);
                                intent.putExtra("tier",tierToPass);
                                intent.putExtra("division",divisionToPass);
                                intent.putExtra("Uid",UidToPass);

                                MainActivity activity = (MainActivity) getActivity();
                                final User userToPass = activity.getUser();
                                if (userToPass.getTeam()!=null){ // Maybe have to change to String.valueOf()
                                    intent.putExtra("team","yes");
                                } else { intent.putExtra("team", "no");}

                                intent.putExtra("MyTeamName", userToPass.getTeam());

                                startActivity(intent);
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                    
                });
                
                save(v,decision); //TODO: 13-Dec-16 ADDED SAVE <---- INSIDE ON BUTTON CLICK EVENT
            }
            
            
        });

        decision = load(v);
        if (decision != null){
            buttonSearch.performClick();
        } else {
            decision = new int[]{0, 0, 0, 0, 0};
            Log.e("decision was null ","@@@@@@@@@@@@@");
        }

        return v;
    }
    
    public void save(View v, int[] decision){
        String data = String.valueOf(decision[0])+"|"+String.valueOf(decision[1])+"|"+String.valueOf(decision[2])+"|"
                +String.valueOf(decision[3])+"|"+String.valueOf(decision[4]);
        try {
            outputStream = new FileOutputStream(file);
            outputStream.write(data.getBytes());
            outputStream.close();
            Toast.makeText(getContext(), "data saved", Toast.LENGTH_SHORT).show();
            
        } catch (IOException e) {
            Log.e("error","not written");
            e.printStackTrace();
        }
    }
    
    public int[] load(View v){
        int length = (int) file.length();
        byte[] bytes = new byte[length];
        try{
            inputStream = new FileInputStream(file);
            inputStream.read(bytes);
            inputStream.close();
            String data = new String(bytes);
            int[] decision = {0,0,0,0,0};
            decision[0] = Integer.parseInt(data.split("\\|")[0]);
            decision[1] = Integer.parseInt(data.split("\\|")[1]);
            decision[2] = Integer.parseInt(data.split("\\|")[2]);
            decision[3] = Integer.parseInt(data.split("\\|")[3]);
            decision[4] = Integer.parseInt(data.split("\\|")[4]);
            Toast.makeText(getContext(),"data loaded",Toast.LENGTH_SHORT).show();
            
            return decision;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("error","no data loaded");
            return null;
        }
    }

}
