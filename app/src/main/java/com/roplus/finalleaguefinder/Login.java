package com.roplus.finalleaguefinder;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Login extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private Button buttonLogin;

    private EditText editTextLoginEmail, editTextLoginPassword;

    private String setAccount, Uid;

    public Login() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextLoginEmail = (EditText) findViewById(R.id.editTextLoginEmail);
        editTextLoginPassword = (EditText) findViewById(R.id.editTextLoginPassword);

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    //User is signed in
                    Log.e(TAG, "onAuthStateChanged:signed_in:"+user.getUid());
                    Uid = user.getUid();
                } else {
                    // User is signed out
                    Log.e(TAG, "onAuthStateChancged:signed_out");
                }
            }
        };

        mAuth.addAuthStateListener(mAuthListener);

        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn(editTextLoginEmail.getText().toString(), editTextLoginPassword.getText().toString());
            }
        });
    }

    private void signIn(final String email, String password){

        Log.d(TAG, "signIn: " + email);

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete: " + task.isSuccessful());

                        if(!task.isSuccessful()){
                            Log.w(TAG, "signInWithEmail:failed", task.getException());
                            Toast.makeText(getBaseContext(), "Authentication failed", Toast.LENGTH_SHORT).show();
                        } else {

                            final int index = email.indexOf("@");

                            Toast.makeText(getBaseContext(), "Authentication succesfull!", Toast.LENGTH_SHORT).show();

                            final FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference myRef = database.getReference("users");

                            myRef.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (DataSnapshot mydata : dataSnapshot.getChildren()) {

                                        User user = mydata.getValue(User.class);
                                        Log.e("getEmail", user.getEmailAddress());
                                        Log.e("email", email.substring(0, index));

                                        if (user.getEmailAddress().equals(email.substring(0,index))) {
                                            Log.e("My csSummonerName", "" + user.getCsSummonerName());
                                            Log.e("My setAccount", "" + user.getSetAccount());

                                            setAccount = user.getSetAccount();

                                            break;
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (setAccount.equals("No")) {
                                        Intent intent = new Intent(getBaseContext(), SetLolAccount.class);
                                        intent.putExtra("email", email.substring(0,index));
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(getBaseContext(), MainActivity.class);
                                        startActivity(intent);
                                    }
                                }
                            },2000);


                        }
                    }
                });

    }

    public void registerOnClick(View view){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
}
