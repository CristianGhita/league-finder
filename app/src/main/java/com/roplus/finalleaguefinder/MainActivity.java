package com.roplus.finalleaguefinder;

import android.content.Intent;
import android.media.Image;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.support.v7.appcompat.R.styleable.Spinner;

public class MainActivity extends AppCompatActivity {

    private RadioButton radioButtonSearch, radioButtonMessages, radioButtonProfile, radioButtonNotifications, radioButtonSettings;
    public ArrayAdapter<CharSequence> adapter;
    private ImageView imageViewText;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private String Uid, name;

    private User user;
    private Team team;

    // TODO: 13-Dec-16 In order for the app to function properly when testing you have to wait for Log to say user and team SET, it can be adjusted by using asynctask instead of handlers 

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioButtonSearch = (RadioButton) findViewById(R.id.radioButtonSearch);
        //radioButtonMessages = (RadioButton) findViewById(R.id.radioButtonMessages);
        radioButtonProfile = (RadioButton) findViewById(R.id.radioButtonProfile);
        radioButtonNotifications = (RadioButton) findViewById(R.id.radioButtonNotifications);
        radioButtonSettings = (RadioButton) findViewById(R.id.radioButonSettings);

        imageViewText = (ImageView) findViewById(R.id.imageViewText);
        imageViewText.setImageResource(R.drawable.leaguefindertext);

        // SET Uid, csSummonerName, etc. to be sent to fragments

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    //User is signed in
                    Log.e("userLoggedIn", user.getUid());
                    Uid = user.getUid();
                } else {
                    // User is signed out
                    Log.e("userLoggedOut", "onAuthStateChancged:signed_out");
                }
            }
        };

        mAuth.addAuthStateListener(mAuthListener);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("users");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot mydata : dataSnapshot.getChildren()) {
                    user = mydata.getValue(User.class);

                    if (user.getUid().equals(Uid)){
                        //name = user.getCsSummonerName();
                        Log.e("user", "SET");
                        Log.e("userName",user.getCsSummonerName());
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try{
                    DatabaseReference myRef2 = database.getReference("teams");
                    myRef2.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for(DataSnapshot mydata : dataSnapshot.getChildren()){
                                team = mydata.getValue(Team.class);

                                if(team.getName().equals(user.getTeam())){
                                    Log.e("team","SET");
                                    Log.e("userTeam",user.getTeam());
                                    break;
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                } catch (Exception e){
                    Log.e("UserTeam","mission or error");
                }

            }
        }, 2000);
    }

    public void displayFragment(View v){

        imageViewText.setImageResource(R.drawable.nullimage);

        int id = v.getId();

        switch(id){

            case R.id.radioButtonSearch:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, new FragmentSearch()).commit();
                break;

            case R.id.radioButtonProfile:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, new FragmentProfile()).commit();
                break;

            case R.id.radioButtonNotifications:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, new FragmentNotifications2()).commit();
                break;

            case R.id.radioButonSettings:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, new FragmentSettings()).commit();
                break;
        }

    }

    public void update(View v){
        getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, new FragmentNotifications2()).commit();
    }

    public String getUid() { return Uid; }
    public String getName() { return name; }
    public User getUser() { return user; }
    public Team getTeam() {
        try{
            return team;
        } catch (Exception e){
            return null;
        }
    }
}
