package com.roplus.finalleaguefinder;

import android.content.Context;
import android.media.Image;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by test on 30-Nov-16.
 */

public class PlayerSearchAdapter extends ArrayAdapter<Summoner>{

    public int resource;
    private ArrayList<Summoner> summoners;
    private Context context;

    public PlayerSearchAdapter(Context context, int resource, ArrayList<Summoner> summoners){
        super(context,resource,summoners);
        this.resource = resource;
        this.summoners = summoners;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View v = convertView;
        try {
            if (v == null){
                LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = layoutInflater.inflate(resource, parent, false);
            }

            ImageView imageView = (ImageView) v.findViewById(R.id.imageViewTierPlayerSearch);
            TextView textViewName = (TextView) v.findViewById(R.id.textViewSummonerNamePlayerSearch);
            TextView textViewTier = (TextView) v.findViewById(R.id.textViewTierDivisionPlayerSearch);

            imageView.setImageResource(summoners.get(position).getPhoto());
            textViewName.setText(summoners.get(position).getName());
            textViewTier.setText(summoners.get(position).getTier());
        } catch (Exception e) {
            e.printStackTrace();
            e.getCause();
        }
        return v;
    }
}
