package com.roplus.finalleaguefinder;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class RegisterActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private EditText editTextEmail, editTextPassword, editTextConfirmPassword;

    String email, password;
    private String TAG = "RegisterActivity", Uid;

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in: "+user.getUid());
                    Uid = user.getUid();
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        editTextEmail = (EditText) findViewById(R.id.editTextRegisterEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextRegisterPassword);
        editTextConfirmPassword = (EditText) findViewById(R.id.editTextRegisterConfirmPassowrd);

        Button buttonRegister = (Button) findViewById(R.id.buttonRegister);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((editTextEmail.getText().toString().contains("@") && (editTextEmail.getText().toString().contains(".co")))
                        && (editTextPassword.getText().toString().equals(editTextConfirmPassword.getText().toString())) && (editTextPassword.getText().toString().length()>5)){
                    email = editTextEmail.getText().toString();
                    password = editTextPassword.getText().toString();

                    int index = email.indexOf("@");
                    mDatabase.child("users").child(Uid).child("Uid").setValue(Uid);
                    mDatabase.child("users").child(Uid).child("emailAddress").setValue(email.substring(0,index));
                    mDatabase.child("users").child(Uid).child("emailDomain").setValue(email.substring(index+1,email.length()));
                    Log.d("EMAAAAAAIL: ", email);
                    mDatabase.child("users").child(Uid).child("setAccount").setValue("No"); // sets that the user is new and has no lol set account

                    //mDatabase.child("users").child(email).setValue("test");

                    Toast.makeText(getBaseContext(), "Account created!", Toast.LENGTH_SHORT).show();



                    createAccount(editTextEmail.getText().toString(),
                            editTextPassword.getText().toString());

                    openLogin();
                }
                else {
                    Toast.makeText(getBaseContext(), "Make sure email and password are correct!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop(){
        super.onStop();
        if (mAuthListener != null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void createAccount(String email, String password){
        Log.d(TAG, "createAccount: " + email);

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete: "+task.isSuccessful());

                        if (!task.isSuccessful()) {
                            Toast.makeText(getBaseContext(), "Register failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void openLogin(){
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }
}
