package com.roplus.finalleaguefinder;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SetLolAccount extends AppCompatActivity {

    private ImageView imageViewTier;

    private TextView textViewRank, textViewCSSummonerName, textViewContinue, textViewWarning;
    private Button buttonSearchSummoner;
    private EditText editTextInputSummonerName;

    public String summonerName, id, csSummonerName, tier, division;
    private String TAG = "SetLolAccount";

    private DatabaseReference mDatabase;

    public String LoginEmail, Uid;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_lol_account);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    Uid = user.getUid();
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };

        mAuth.addAuthStateListener(mAuthListener);

        LoginEmail = this.getIntent().getStringExtra("email");
        Log.d("@@@@@ EMAIL: ", LoginEmail);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        imageViewTier = (ImageView) findViewById(R.id.imageViewTier);
        textViewRank = (TextView) findViewById(R.id.textViewRank);
        textViewCSSummonerName = (TextView) findViewById(R.id.textViewCSSummonerName);

        textViewContinue = (TextView) findViewById(R.id.textViewContinue);
        textViewWarning = (TextView) findViewById(R.id.textViewWarning);

        editTextInputSummonerName = (EditText) findViewById(R.id.editTextInputSummonerName);

        buttonSearchSummoner = (Button) findViewById(R.id.buttonSearchSummoner);
        buttonSearchSummoner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextInputSummonerName.getText().toString() != null){

                    editTextInputSummonerName.setText(editTextInputSummonerName.getText().toString().replace(" ",""));
                    summonerName = editTextInputSummonerName.getText().toString().toLowerCase();

                    new JSONGetID().execute("https://eune.api.pvp.net/api/lol/eune/v1.4/summoner/by-name/" + summonerName + "?api_key=RGAPI-7d989743-3f36-4c2d-bac3-925b20d0b8ea");
                } else {
                    Toast.makeText(getBaseContext(), "Please enter summoner name", Toast.LENGTH_SHORT).show();;
                }
            }
        });
    }

    public class JSONGetID extends AsyncTask<String, String, String>{

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            URL url = null;

            try {
                url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while((line = reader.readLine()) != null){
                    buffer.append(line);
                }

                String finalJson = buffer.toString();
                JSONObject parentObject = new JSONObject(finalJson);
                JSONObject finalObject = parentObject.getJSONObject(String.valueOf(summonerName));
                int idObject = finalObject.getInt("id");
                String nameObject = finalObject.getString(String.valueOf("name"));

                id = ""+idObject;
                csSummonerName = nameObject;


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            //mDatabase.child("users").child(LoginEmail).child("csSummonerName").setValue(csSummonerName);
            //mDatabase.child("users").child(LoginEmail).child("id").setValue(id);
            new JSONGetRank().execute("https://eune.api.pvp.net/api/lol/eune/v2.5/league/by-summoner/" + id + "/entry?api_key=RGAPI-7d989743-3f36-4c2d-bac3-925b20d0b8ea");
        }

    }

    public class JSONGetRank extends AsyncTask<String, String, String>{

        @Override
        protected String doInBackground(String... params) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            URL url = null;
            try {
                url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";
                while((line = reader.readLine()) != null){
                    buffer.append(line);
                }

                String finalJson = buffer.toString();

                JSONObject parentObject = new JSONObject(finalJson);
                JSONArray idArray = parentObject.getJSONArray(id);
                JSONObject soloObject = idArray.getJSONObject(0);
                tier = soloObject.getString(String.valueOf("tier"));
                JSONArray entriesArray = soloObject.getJSONArray("entries");
                JSONObject soloDuoObject = entriesArray.getJSONObject(0);
                division = soloDuoObject.getString(String.valueOf("division"));

                return null;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (connection != null){
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            setData(csSummonerName, tier, division);
        }
    }

    private void setData(final String csSummonerName, final String tier, final String division){

        switch (tier){
            case "BRONZE":
                imageViewTier.setImageResource(R.drawable.bronze);
                break;
            case "SILVER":
                imageViewTier.setImageResource(R.drawable.silver);
                break;
            case "GOLD":
                imageViewTier.setImageResource(R.drawable.gold);
                break;
            case "PLATINUM":
                imageViewTier.setImageResource(R.drawable.platinum);
                break;
            case "DIAMOND":
                imageViewTier.setImageResource(R.drawable.diamond);
                break;
            case "MASTER":
                imageViewTier.setImageResource(R.drawable.master);
                break;
            case "CHALLENGER":
                imageViewTier.setImageResource(R.drawable.challenger);
                break;
            default:
                imageViewTier.setImageResource(R.drawable.provisional);
        }

        Log.e("UID @@@@@", Uid);
        textViewCSSummonerName.setText(csSummonerName);
        textViewRank.setText(tier+" "+division);
        textViewContinue.setText("Continue");
        textViewContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int idInt;
                idInt = Integer.parseInt(id);
                mDatabase.child("users").child(Uid).child("csSummonerName").setValue(csSummonerName);
                mDatabase.child("users").child(Uid).child("id").setValue(idInt);
                mDatabase.child("users").child(Uid).child("tier").setValue(tier);
                mDatabase.child("users").child(Uid).child("division").setValue(division);
                mDatabase.child("users").child(Uid).child("setAccount").setValue("Yes");

                Intent intent = new Intent(SetLolAccount.this, MainActivity.class);
                startActivity(intent);

            }
        });
        textViewWarning.setText("Please be sure your account is correct.");

    }

}
