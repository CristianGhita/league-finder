package com.roplus.finalleaguefinder;

/**
 * Created by test on 30-Nov-16.
 */

public class Summoner {

    private String name;
    private String tier;
    private int photo;

    public Summoner(String name, String tier, int photo){
        this.name = name;
        this.tier = tier;
        this.photo = photo;
    }

    public String getName(){
        return name;
    }

    public String getTier(){
        return tier;
    }

    public int getPhoto(){
        return photo;
    }

    @Override
    public String toString() {
        return tier;
    }
}
