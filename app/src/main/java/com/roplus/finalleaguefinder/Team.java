package com.roplus.finalleaguefinder;

import com.google.firebase.database.GenericTypeIndicator;

import java.lang.reflect.Array;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by test on 05-Dec-16.
 */

public class Team {

    private String name, captain;
    private int size;
    private ArrayList<String> members;

    public Team(String captain, ArrayList<String> members, String name, int size){
        this.name = name;
        this.captain = captain;
        this.size = size;
        this.members = members;
    }

    public String getName() { return name; }
    public String getCaptain() { return captain; }
    public int getSize() { return size; }
    public ArrayList<String> getMembers() { return members; }

    public Team(){

    }
}
