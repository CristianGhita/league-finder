package com.roplus.finalleaguefinder;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by test on 25-Nov-16.
 */

public class User {

    private String Uid, emailDomain, setAccount, csSummonerName, division, tier, emailAddress, team, invites;
    private int id;

    public User(String Uid, String emailDomain, int id, String setAccount, String csSummonerName, String division, String tier, String emailAddress, String team, String invites){
        this.Uid = Uid;
        this.emailDomain = emailDomain;
        this.setAccount = setAccount;
        this.csSummonerName = csSummonerName;
        this.division = division;
        this.tier = tier;
        this.id = id;
        this.emailAddress = emailAddress;
        this.team = team;
        this.invites = invites;
    }

    public String getUid() { return Uid; }
    public String getEmailDomain() { return emailDomain; }
    public String getSetAccount() { return setAccount; }
    public String getCsSummonerName() { return csSummonerName; }
    public String getDivision() { return division; }
    public String getTier() { return tier; }
    public int getIdBack() { return id; }
    public String getEmailAddress() { return emailAddress; }
    public String getTeam() { return team; }
    public String getInvites() { return  invites; }

    public User(){

    }

}
