package com.roplus.finalleaguefinder;

/**
 * Created by test on 03-Dec-16.
 */

public class UserChild {

    private String key;
    private String[] value;

    public UserChild(String Key, String[] value){
        this.key = Key;
        this.value = value;
    }

    public String getKey(){
        return key;
    }

    public String[] getValue(){
        return value;
    }

}
